import os

import numpy
import gmsh


if __name__ == "__main__":
    
    # Initialize gmsh
    gmsh.initialize()
    gmsh.model.add("HollowPlate")
    
    # Import .stp model
    gmsh.model.occ.importShapes(os.getcwd() + r"//geometry//4.01.HOLLOW_PLATE.step")
    
    # Find point for imposing boundaries dx dy dz
    point_dxdydz = gmsh.model.occ.getEntitiesInBoundingBox(100-1, 50-1, 10-1, 100+1, 50+1, 10+1)
    gmsh.model.occ.synchronize()
    point_dxdydz_group = gmsh.model.add_physical_group(0, [point_dxdydz[0][1]])
    gmsh.model.setPhysicalName(0, point_dxdydz_group, "point_dxdydz")
    
    # Find point for imposing boundaries dz
    point_dz = gmsh.model.occ.getEntitiesInBoundingBox(100-1, -50-1, 10-1, 100+1, -50+1, 10+1)
    gmsh.model.occ.synchronize()
    point_dz_group = gmsh.model.add_physical_group(0, [point_dz[0][1]])
    gmsh.model.setPhysicalName(0, point_dz_group, "point_dz")
    
    # Find face for imposing boundaries dx
    face_dx = gmsh.model.occ.getEntitiesInBoundingBox(100-1, -50-1, 0-1, 100+1, 50+1, 10+1, 2)
    gmsh.model.occ.synchronize()
    face_dx_group = gmsh.model.add_physical_group(2, [face_dx[0][1]])
    gmsh.model.setPhysicalName(2, face_dx_group, "face_dx")
    
    # Find face for imposing force fx
    face_fx = gmsh.model.occ.getEntitiesInBoundingBox(-100-1, -50-1, 0-1, -100+1, 50+1, 10+1, 2)
    gmsh.model.occ.synchronize()
    face_fx_group = gmsh.model.add_physical_group(2, [face_fx[0][1]])
    gmsh.model.setPhysicalName(2, face_fx_group, "face_fx")

    all_solids = gmsh.model.occ.getEntities(3)
    gmsh.model.occ.synchronize()
    all_solids_group = gmsh.model.add_physical_group(3, [all_solids[0][1]])
    gmsh.model.setPhysicalName(3, all_solids_group, "all_solids")

    # Synchonize gmsh
    gmsh.model.occ.synchronize()
    # Generate meshes with various density
    for order in [1,2]:
        for size in [20, 10, 5, 2.5]:    
            # Set global mesh size
            gmsh.option.setNumber("Mesh.MeshSizeMin", size)
            gmsh.option.setNumber("Mesh.MeshSizeMax", size)
            # Generate mesh
            gmsh.model.mesh.generate(3)
            # Set mesh order
            gmsh.model.mesh.setOrder(order)
            # Save generated mesh
            gmsh.write(f"mesh//HollowPlateMesh_order{order}_{str(size).replace('.','_')}.med")
            # Clear mesh
            gmsh.model.mesh.clear()
    # Run gmsh (optionally)
    gmsh.fltk.run()
    # Close gmsh session
    gmsh.finalize()