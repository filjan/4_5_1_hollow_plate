import numpy as np 
import matplotlib.pyplot as plt

if __name__=="__main__":
    
    displacements = np.genfromtxt(r"./results_tables/results_dx.csv", delimiter=",")
    stresses_no = np.genfromtxt(r"./results_tables/results_sv.csv", delimiter=",")
    stresses_el = np.genfromtxt(r"./results_tables/results_se.csv", delimiter=",")
    
    number_of_loop = displacements[:,0]
    number_of_nodes = displacements[:,1]
    displacement_dx = displacements[:,2]
    stress_no = stresses_no[:,2]
    stress_el = stresses_el[:,2]
    
    plt.figure()
    plt.title("Mesh convergence test")
    plt.xlabel("Number of nodes")
    plt.ylabel("Averaged displacement DX of the face 'face_fx'")
    plt.plot(number_of_nodes[:4], displacement_dx[:4], marker = "o", linestyle="-", label="1 order elements")
    plt.plot(number_of_nodes[4:], displacement_dx[4:], marker = "X", linestyle="-.", label="2 order elements")
    plt.grid()
    plt.legend()
    
    plt.figure()
    plt.title("Mesh singularity")
    plt.xlabel("Number of nodes")
    plt.ylabel("Maximum VMis stress in the region of hole-notch")
    plt.plot(number_of_nodes[:4], stress_no[:4], marker = "o", linestyle="-", label="1 order elements - Nodes")
    plt.plot(number_of_nodes[4:], stress_no[4:], marker = "X", linestyle="-", label="2 order elements - Nodes")

    plt.plot(number_of_nodes[:4], stress_el[:4], marker = "o", linestyle="-.", label="1 order elements - Elements")
    plt.plot(number_of_nodes[4:], stress_el[4:], marker = "X", linestyle="-.", label="2 order elements - Elements")

    plt.grid()
    plt.legend()   
    
    plt.show()