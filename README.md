
# How to run example in a few steps:
1. Download repository
2. Run mesh_generator.py in its directory
3. Open .astk in ASTK (in Salome_Meca->Tools->Plugins->Code_aster->Run astk)
4. Modify path to your own path in .astk file
5. Open .comm file and change comm_file_path variable in
the beginning of the file to your own path. Optionally replace True of False in interesting files to return (3 dimensional values distribution and tables)
5. Run calculations
6. Run plot_postprocessing_generator.py

---
# Example explanation
Hi!

Here you can find input files, mesh generator and solutions of example "Hollow Plate" ch.4.5.1 from Mr. Paul M. Kurowski Book "Finite Element Analysis for Design Engineers Second Edition" 2017 SAE International. Some of pictures are used from this book, and some are self-made.

### Geometrical model:  
length = 200 mm  
thickness = 10 mm  
diameter = 40 mm  
height = 100 mm
![Scheme](https://bitbucket.org/filjan/hollowplate/raw/main/images/HollowPlateGeometricalModel.png)

### Mesh types
Author suggests using 4 types mesh varied element density:
20 mm
10 mm
5 mm
2.5 mm
You can generate each mesh with mesh generator with python:
On the left you can see 1 order element meshes, on the right side 2 order.

![Kiku](images/HollowPlateMeshes.png)  

Example of isometry view:

![Kiku](images/HollowPlateMeshIsometry.png)  
### Boundary conditions
Picture below shows boundary conditions:  
Nodes on the right side are restrainted in direction normal to surface(i.e. dx).  
Node marked with blue arrows is restricted in each direction (dx, dy,dz).  
Node marked with green arrow is restricted in one direction (dz).  
Node marked with pink arrows are influenced by force of FX=-100kN
![Kiku](images/HollowPlateBoundaries.png)  
(Picture from Mr. Kurowski book)

Geometrical representation in Gmsh with marked boundary areas:  
![Kiku](images/HollowPlateMeshGmshGeo.png)  

The best way to exert even force out-of whole plane is to use pressure boundary condition. Here 100_000 N/(thickness*height) = 100 MPa.

Example map of vonMises stress:  
![Kiku](images/HollowPlateVonMises.png)  

Example map of DX displacement (Scale factor equals 100):  
![Kiku](images/HollowPlateDX.png)  

### Chief points of example

#### Conduct mesh convergence test acc. to whole displacement of right face.

Figure of mesh convergence:  
![Kiku](images/HollowPlateDXConvergence.png)  

Each consecutive pair of 1 order and 2 order meshes, have same number of elements.

#### Reveal stress singularities in notch area
![Kiku](images/HollowPlateVonMisesSingularity.png)  
'Nodes' label means values extrapolated to nodes.  
'Elements' label means values averaged in element from Gauss.

#### Prove difficulties in revealing stress singularities in coarse meshes.    
Extra example relies upon imposing displacement 0 in each direction thoroughly on 'face_dx'.

Picture below shows the titlementioned phenomen:
![Kiku](images/HollowPlateVonMisesRestraintSingularity.png)  
Upper view - 2 order, 2,5mm mesh
Bottom view - 1 order, 10mm mesh

Same picture as above but with visible elements edges:
![Kiku](images/HollowPlateVonMisesRestraintSingularityEdges.png)  


-----

-----
